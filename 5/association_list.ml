let rec assoc a = function
    (a', b) :: rest -> 
    if a = a' then b else assoc a rest;;

let rec assoc2 a = function
    (a', b) :: rest -> if a = a' then b else assoc a rest
  | [] -> ""
