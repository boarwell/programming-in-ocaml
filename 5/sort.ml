let nextrand seed =
  let a = 16807. and m = 2147483647. in
  let t = a *. seed in 
  t -. m *. floor (t /. m);;

let rec randlist n seed tail =
  if n = 0 then (seed, tail)
  else randlist (n - 1) (nextrand seed) (seed :: tail);;

let rec insert x = function
    [] -> [x]
  | y :: rest when x < y -> x :: (y :: rest)
  | y :: rest -> y :: (insert x rest);;

(*この辺から理解があやしい*)
let rec insertion_sort = function
    [] -> []
  | x :: rest -> insert x (insertion_sort rest);;

(*partition, quick_sortはちゃんと理解できていません*)
let rec partition pivot = function
    [] -> ([], [])
  | y :: rest ->
    let (left, right) = partition pivot rest in 
    if pivot < y then (left, y :: right)
    else (y :: left, right);; 

let rec quick_sort = function
    [] -> []
  | pivot :: rest ->
    let (left ,right) = partition pivot rest in 
    quick_sort left @ (pivot :: quick_sort right);;

(*こっちのほうが最初のpartitionよりも理解しやすい*)
let rec partition2 left right pivot = function
    [] -> (left, right)
  | y :: rest ->
    if pivot < y then partition2 left (y :: right) pivot rest
    else partition2 (y :: left) right pivot rest;;

(*これはなかなか手ごわい*)
(*読んで理解はできるが自分では書けない*)
let rec quick_sort2 = function
    [] -> []
  | pivot :: rest ->
    let rec partition3 left right = function
        [] -> (left, right)
      | y :: ys -> 
        if pivot < y then partition3 left (y :: right) ys
        else partition3 (y :: left) right ys
    in
    let (left, right) = partition3 [] [] rest in 
    (quick_sort2 left) @ (pivot :: quick_sort2 right);;

