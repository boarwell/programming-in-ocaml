let x = 2 and y = 1;;
print_endline (string_of_int x);;
print_endline (string_of_int y);;

(* 同時定義の場合は=の右辺はそこで宣言されるどの変数の有効範囲にも入らない *)
(* 最初に定義した、x=2, y=1 を参照する*)
let x = y and y = x;;
print_endline (string_of_int x);;
print_endline (string_of_int y);;