let rec pow (x, n) =
  if n = 0 then
    1.
  else
    x *. pow(x, n - 1);;

let pow1 (x, n) =
  let rec f (tmp, i) =
    if i = n then
      tmp
    else
      f (tmp * x, i + 1)
  in 
  f (x, 1);;

(*Printf.printf "%d\n" (pow1 (2, 5));;*)

let rec pow2 (x, n) =
  if n = 1 then
    x
  else if n mod 2 = 0 then
    pow2 (x * x, n / 2)
  else
    (pow2 (x * x, ((n + 1) / 2))) / x;;

Printf.printf "%d\n" (pow2 (2, 8));;
Printf.printf "%d\n" (pow2 (2, 9));;
Printf.printf "%d\n" (pow2 (2, 10));;

